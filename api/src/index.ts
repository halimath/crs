export interface CharacterInfoDto {
    id: string,
    name: string,
    species: string,
    careers: Array<CareerDto>,
}

export interface CareerDto {
    name: string,
    specialization: string,
}

export interface CharacterDto {
    info: CharacterInfoDto,
    characteristics: CharacteristicsDto,
    skills: SkillsDto,
}

export interface CharacteristicsDto {
    brawn: number,
    agility: number,
    intellect: number,
    cunning: number,
    willpower: number,
    presence: number,
}

export interface SkillsDto {
    general: Array<SkillDto>,
    combat: Array<SkillDto>,
    knowledge: Array<SkillDto>,
}

export interface SkillDto {
    id: string,
    career: boolean,
    characteristic: CharacteristicKind,
    rank: number,
}

export enum CharacteristicKind {
    Brawn = "brawn",
    Agility = "agility",
    Intellect = "intellect",
    Cunning = "cunning",
    Willpower = "willpower",
    Presence = "presence",
}
