package dto

import "bitbucket.org/halimath/crs/backend/model"

type CharacterInfoDTO struct {
	ID      string      `json:"id"`
	Name    string      `json:"name"`
	Species string      `json:"species"`
	Careers []CareerDTO `json:"careers"`
}

type CareerDTO struct {
	Name           string `json:"name"`
	Specialization string `json:"specialization"`
}

type CharacterDTO struct {
	Info            CharacterInfoDTO                  `json:"info"`
	Characteristics map[model.CharacteristicKind]uint `json:"characteristics"`
	Skills          map[string][]SkillDTO             `json:"skills"`
}

type SkillDTO struct {
	ID             string                   `json:"id"`
	Career         bool                     `json:"career"`
	Characteristic model.CharacteristicKind `json:"characteristic"`
	Rank           uint                     `json:"rank"`
}

func ConvertCharacter(m *model.Character) CharacterDTO {
	return CharacterDTO{
		Info:            ConvertInfo(&m.Info),
		Characteristics: ConvertCharacteristics(&m.Characteristics),
		Skills:          ConvertSkills(&m.Skills),
	}
}

func ConvertSkills(m *model.Skills) map[string][]SkillDTO {
	return map[string][]SkillDTO{
		"general":   ConvertSliceOfSkills(m.General),
		"combat":    ConvertSliceOfSkills(m.Combat),
		"knowledge": ConvertSliceOfSkills(m.Knowledge),
	}
}

func ConvertSliceOfSkills(m []model.Skill) []SkillDTO {
	r := make([]SkillDTO, len(m))
	for idx, s := range m {
		r[idx] = ConvertSkill(s)
	}
	return r
}

func ConvertSkill(m model.Skill) SkillDTO {
	return SkillDTO{
		ID:             m.ID,
		Career:         m.Career,
		Characteristic: m.Characteristic,
		Rank:           m.Rank,
	}
}

func ConvertCharacteristics(m *model.Characteristics) map[model.CharacteristicKind]uint {
	return map[model.CharacteristicKind]uint{
		model.CharacteristicBrawn:     m.Brawn,
		model.CharacteristicAgility:   m.Agility,
		model.CharacteristicIntellect: m.Intellect,
		model.CharacteristicCunning:   m.Cunning,
		model.CharacteristicWillpower: m.Willpower,
		model.CharacteristicPresence:  m.Presence,
	}
}

func ConvertInfos(m []*model.CharacterInfo) []CharacterInfoDTO {
	r := make([]CharacterInfoDTO, len(m))
	for i, c := range m {
		r[i] = ConvertInfo(c)
	}
	return r
}

func ConvertInfo(info *model.CharacterInfo) CharacterInfoDTO {
	return CharacterInfoDTO{
		ID:      info.ID,
		Name:    info.Name,
		Species: info.Species,
		Careers: ConvertCareers(info.Careers),
	}
}

func ConvertCareers(m []model.Career) []CareerDTO {
	r := make([]CareerDTO, len(m))
	for i, c := range m {
		r[i] = ConvertCareer(c)
	}
	return r
}

func ConvertCareer(m model.Career) CareerDTO {
	return CareerDTO{
		Name:           m.Name,
		Specialization: m.Specialization,
	}
}
