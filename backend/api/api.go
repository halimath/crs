package api

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"bitbucket.org/halimath/crs/backend/api/dto"
	"bitbucket.org/halimath/crs/backend/service"
	"bitbucket.org/halimath/kvlog"
)

func Provide(srv service.Service) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Use(middleware.Recoverer)

		r.Get("/players/{playerID}/characters", getCharactersForPlayer(srv))
		r.Get("/characters/{characterID}", getCharacter(srv))
	})

	return r
}

func getCharacter(srv service.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "characterID")
		c, err := srv.FindCharacterByID(r.Context(), id)
		if err != nil {
			if err == service.ErrNotFound {
				http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
			} else {
				kvlog.Error(kvlog.KV("event", "errorLoadingCharacter"), kvlog.KV("error", err))
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
			return
		}

		sendJSON(w, dto.ConvertCharacter(c), http.StatusOK)
	}
}

func getCharactersForPlayer(srv service.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		playerID := chi.URLParam(r, "playerID")

		ctx := r.Context()

		infos, err := srv.FindCharacterInfosByPlayerID(ctx, playerID)
		if err != nil {
			kvlog.Error(kvlog.KV("event", "errorLoadingCharactersForPlayer"), kvlog.KV("error", err))
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		sendJSON(w, dto.ConvertInfos(infos), http.StatusOK)
	}
}

func sendJSON(w http.ResponseWriter, payload interface{}, statusCode int) {
	jsonBytes, err := json.Marshal(payload)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	header := w.Header()
	header.Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if _, err := w.Write(jsonBytes); err != nil {
		kvlog.Error(kvlog.KV("event", "errorWritingJSONResponse"), kvlog.KV("error", err))
	}
}
