package model

type CharacterInfo struct {
	ID       string
	PlayerID string
	Name     string
	Species  string
	Careers  []Career
}

type Career struct {
	Name           string
	Specialization string
}

type Character struct {
	Info            CharacterInfo
	Characteristics Characteristics
	Skills          Skills
}

type Characteristics struct {
	Brawn, Agility, Intellect, Cunning, Willpower, Presence uint
}

type CharacteristicKind string

const (
	CharacteristicBrawn     CharacteristicKind = "brawn"
	CharacteristicAgility   CharacteristicKind = "agility"
	CharacteristicIntellect CharacteristicKind = "intellect"
	CharacteristicCunning   CharacteristicKind = "cunning"
	CharacteristicWillpower CharacteristicKind = "willpower"
	CharacteristicPresence  CharacteristicKind = "presence"
)

type Skill struct {
	ID             string
	Career         bool
	Characteristic CharacteristicKind
	Rank           uint
}

type Skills struct {
	General, Combat, Knowledge []Skill
}
