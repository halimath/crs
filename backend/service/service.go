package service

import (
	"context"
	"errors"

	"bitbucket.org/halimath/crs/backend/model"
)

var data []model.Character

func init() {
	data = []model.Character{
		{
			Info: model.CharacterInfo{
				ID:       "00000000-0000-0000-0000-000000000011",
				PlayerID: "00000000-0000-0000-0000-000000000001",
				Name:     "Pi",
				Species:  "Mensch",
				Careers: []model.Career{
					{
						Name:           "Techniker",
						Specialization: "Hacker",
					},
					{
						Name:           "Schmuggler",
						Specialization: "Pilot",
					},
				},
			},
			Characteristics: model.Characteristics{
				Brawn:     2,
				Agility:   3,
				Intellect: 3,
				Cunning:   3,
				Willpower: 2,
				Presence:  2,
			},
			Skills: model.Skills{
				General: []model.Skill{
					{ID: "astronavigation", Characteristic: model.CharacteristicIntellect, Career: true, Rank: 1},
					{ID: "athletics", Characteristic: model.CharacteristicBrawn, Career: false, Rank: 1},
					{ID: "charm", Characteristic: model.CharacteristicPresence, Career: false, Rank: 0},
					{ID: "coercion", Characteristic: model.CharacteristicWillpower, Career: false, Rank: 0},
					{ID: "computers", Characteristic: model.CharacteristicIntellect, Career: true, Rank: 4},
					{ID: "cool", Characteristic: model.CharacteristicPresence, Career: true, Rank: 1},
					{ID: "coordination", Characteristic: model.CharacteristicAgility, Career: true, Rank: 0},
					{ID: "deception", Characteristic: model.CharacteristicCunning, Career: true, Rank: 1},
					{ID: "discipline", Characteristic: model.CharacteristicWillpower, Career: true, Rank: 1},
					{ID: "leadership", Characteristic: model.CharacteristicPresence, Career: false, Rank: 0},
					{ID: "mechanics", Characteristic: model.CharacteristicIntellect, Career: true, Rank: 2},
					{ID: "medicine", Characteristic: model.CharacteristicIntellect, Career: false, Rank: 0},
					{ID: "negotiation", Characteristic: model.CharacteristicPresence, Career: false, Rank: 0},
					{ID: "perception", Characteristic: model.CharacteristicCunning, Career: true, Rank: 2},
					{ID: "piloting-planetary", Characteristic: model.CharacteristicAgility, Career: true, Rank: 3},
					{ID: "piloting-space", Characteristic: model.CharacteristicAgility, Career: true, Rank: 3},
					{ID: "resilience", Characteristic: model.CharacteristicBrawn, Career: false, Rank: 0},
					{ID: "skulduggery", Characteristic: model.CharacteristicCunning, Career: false, Rank: 0},
					{ID: "stealth", Characteristic: model.CharacteristicAgility, Career: true, Rank: 3},
					{ID: "streetwise", Characteristic: model.CharacteristicCunning, Career: true, Rank: 0},
					{ID: "survival", Characteristic: model.CharacteristicCunning, Career: false, Rank: 0},
					{ID: "vigilance", Characteristic: model.CharacteristicWillpower, Career: false, Rank: 0},
				},
				Combat: []model.Skill{
					{ID: "brawl", Characteristic: model.CharacteristicBrawn, Career: false, Rank: 0},
					{ID: "gunnery", Characteristic: model.CharacteristicAgility, Career: true, Rank: 2},
					{ID: "melee", Characteristic: model.CharacteristicBrawn, Career: false, Rank: 0},
					{ID: "ranged-light", Characteristic: model.CharacteristicAgility, Career: false, Rank: 3},
					{ID: "ranged-heavy", Characteristic: model.CharacteristicAgility, Career: false, Rank: 1},
				},
				Knowledge: []model.Skill{
					{ID: "core-worlds", Characteristic: model.CharacteristicIntellect, Career: false, Rank: 0},
					{ID: "education", Characteristic: model.CharacteristicIntellect, Career: true, Rank: 1},
					{ID: "lore", Characteristic: model.CharacteristicIntellect, Career: false, Rank: 0},
					{ID: "outer-rim", Characteristic: model.CharacteristicIntellect, Career: true, Rank: 0},
					{ID: "underworld", Characteristic: model.CharacteristicIntellect, Career: true, Rank: 0},
				},
			},
		},
	}
}

var (
	ErrNotFound = errors.New("not found")
)

type Service interface {
	FindCharacterInfosByPlayerID(ctx context.Context, playerID string) ([]*model.CharacterInfo, error)
	FindCharacterByID(ctx context.Context, id string) (*model.Character, error)
}

func Provide() Service {
	return &service{}
}

type service struct{}

var _ Service = &service{}

func (s *service) FindCharacterInfosByPlayerID(ctx context.Context, playerID string) ([]*model.CharacterInfo, error) {
	result := make([]*model.CharacterInfo, 0)
	for _, c := range data {
		if c.Info.PlayerID == playerID {
			result = append(result, &c.Info)
		}
	}
	return result, nil
}

func (s *service) FindCharacterByID(ctx context.Context, id string) (*model.Character, error) {
	for _, c := range data {
		if c.Info.ID == id {
			return &c, nil
		}
	}

	return nil, ErrNotFound
}
