package main

import (
	"fmt"
	"net/http"
	"os"

	"bitbucket.org/halimath/kvlog"

	"bitbucket.org/halimath/crs/backend/api"
	"bitbucket.org/halimath/crs/backend/service"
)

func main() {
	srv := service.Provide()
	router := api.Provide(srv)

	server := http.Server{
		Addr:    ":9998",
		Handler: kvlog.Handler(kvlog.L, router),
	}

	kvlog.Info(kvlog.KV("event", "started"), kvlog.KV("address", ":9998"))
	if err := server.ListenAndServe(); err != nil {
		fmt.Fprintf(os.Stderr, "Failed to start server: %s", err)
		os.Exit(1)
	}
}
