module bitbucket.org/halimath/crs/backend

go 1.15

require (
	bitbucket.org/halimath/kvlog v0.2.0
	github.com/go-chi/chi v1.5.1
)
