import * as wecco from "@wecco/core"

import {Message} from "../controller"

export interface AppShellOptions {
    title?: string,
    showBack?: boolean,
}

export function appShell(main: wecco.ElementUpdate, context: wecco.AppContext<Message>, opts?: AppShellOptions): wecco.ElementUpdate {
    return wecco.html`
    ${header(context, opts)}
    <main>
        ${main}
    </main>
    ${footer()}
    `
}

function header(context: wecco.AppContext<Message>, opts?: AppShellOptions) {
    return wecco.html`
        <nav class="navbar sticky-top navbar-dark bg-dark">
            ${opts?.showBack ? wecco.html`<a href="#" @click=${() => context.emit({message: "back"})}><span class="material-icons">arrow_back_ios</span></a>` : ""}
            <a class="navbar-brand" href="/">CRS</a>
            ${opts?.title ? wecco.html`<span class="navbar-text">${opts.title}</span>` : ""}            
        </nav>
    `
}

function footer() {
    return wecco.html`
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                    </div>
                </div>
            </div>
        </footer>
    `
}
