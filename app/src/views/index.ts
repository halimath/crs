import * as wecco from "@wecco/core"
import * as models from "../models"
import * as controller from "../controller"

import { m } from "../i18n"
import { appShell } from "./appshell"
import { character } from "./character"

export function root(model: models.Model, context: wecco.AppContext<controller.Message>): wecco.ElementUpdate {
    if (model.character) {
        return character(model.character, context)
    }

    if (model.characterInfos) {
        return characterInfos(model.characterInfos, context)
    }

    return appShell(wecco.html`
    <p class="text-center">
        ${m("loading")}            
    </p>
    `, context)
}

function characterInfos(infos: Array<models.CharacterInfo>, context: wecco.AppContext<controller.Message>) {
    const title = m("characterlist")
    const main = wecco.html`
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4>${title}</h4>
            </div>
        </div>
        <div class="row">
            ${infos.map(c => characterInfo(c, context))}
        </div>
    </div>
    `

    return appShell(main, context, {
        title: title
    })
}

function characterInfo(info: models.CharacterInfo, context: wecco.AppContext<controller.Message>) {
    return wecco.html`
        <div class="col-6 col-sm-4 col-lg-3">
            <div class="card">
                <a @click=${() => context.emit({message: "load-character", id: info.id})}>
                <div class="card-body">
                    <h5 class="card-title">${info.name}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">${info.species}</h6>
                    <p class="card-text">${info.careers.map(c => c.name).join("; ")}</h5>
                </div>
                </a>
            </div>
        </div>
    `
}