import * as wecco from "@wecco/core"

import {Message} from "../controller"
import { appShell } from "./appshell"
import * as models from "../models"
import { m } from "../i18n"

export function character(model: models.Character, context: wecco.AppContext<Message>) {
    const main = wecco.html`
    <div class="container">
        ${characteristics(model.characteristics)}
        ${skills(model.skills)}
        ${characterInfo(model.info)}  
    </div>    
    `

    return appShell(main, context, {
        title: model.info.name,
        showBack: true,
    })
}

function skills(model: models.Skills) {
    return wecco.html`
        ${sectionHeading(m("character.skills"))}
        <div class="row">
            <div class="col-12 col-lg-6 mt-2">
                <h5>${m("character.skills.general")}</h5>
                ${model.general.map(skill)}
            </div>
            <div class="col-12 col-lg-6 mt-2">
                <h5>${m("character.skills.combat")}</h5>
                ${model.combat.map(skill)}

                <h5 class="mt-2">${m("character.skills.knowledge")}</h5>
                ${model.knowledge.map(skill)}
            </div>
        </div>
    `
}

function skill(model: models.Skill) {
    return wecco.html`
        <div class="clearfix border-bottom">
            <span class="material-icons align-middle">${model.career ? "lens" : "panorama_fish_eye"}</span>
            <span>                
                ${m(`character.skills.${model.id}`)} (${m(`character.characteristics.${model.characteristic.kind}.abr`)})                                
            </span>
            <span class="float-right">
                ${model.rank} <a target="_blank" href="${dicePoolUrl(model.dicePool)}"><span class="material-icons align-middle">casino</span></a>
            </span>
        </div>
    `
}

function characterInfo(model: models.CharacterInfo) {
    return wecco.html`
    ${sectionHeading(m("character.info"))}
    <div class="row">
        <div class="col-6 col-sm-8">
            <p class="small">${m("character.info.name")}</p>
            <p class="lead">${model.name}</p>
        </div>
        <div class="col-6 col-sm-4">
            <p class="small">${m("character.info.species")}</p>
            <p class="lead">${model.species}</p>
        </div>
    </div>      
    `
}

function characteristics(characteristics: Array<models.Characteristic>): wecco.ElementUpdate {
    return wecco.html`
        ${sectionHeading(m("character.characteristics"))}
        <div class="row">
            ${characteristics.map(c => characteristic(c))}
        </div>`
}

function characteristic(c: models.Characteristic): wecco.ElementUpdate {
    return wecco.html`
    <div class="col-6 col-sm-4 col-md-4 col-lg-2">
        <div class="border p-1 text-center">
            <p class="text-truncate">${m(`character.characteristics.${c.kind}`)}
                <span class="float-right">
                    <a target="_blank" href=${dicePoolUrl({ ability: c.rank })} class=""><span class="material-icons">casino</span></a>
                </span>            
            </p>
            <p class="lead font-weight-bold">${c.rank}</p>
        </div>
    </div>
    `
}

function sectionHeading(text: string) {
    return wecco.html`
    <div class="row mt-2">
        <div class="col-12 text-center">
            <h4>${text}</h4>
        </div>
    </div>    
    `
}

function dicePoolUrl(pool: models.DicePool) {
    return "https://diceroller.wilanthaou.de#"
        + repeatChar("A", pool.ability)
        + repeatChar("P", pool.proficiency)
        + repeatChar("D", pool.difficulty)
        + repeatChar("C", pool.challenge)
        + repeatChar("B", pool.boost)
        + repeatChar("S", pool.setback)
        + repeatChar("F", pool.force)
}

function repeatChar(char: string, count?: number): string {
    let result = ""

    for (let i = 0; i < (count ?? 0); ++i) {
        result += char
    }

    return result
}