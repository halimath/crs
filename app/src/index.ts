import * as wecco from "@wecco/core"

import * as models from "./models"
import { update} from "./controller"
import { root } from "./views"
import { loadCharacterInfos } from "./api"

document.addEventListener("DOMContentLoaded", async () => {
    const app = wecco.app(initializeModel, update, root, "#app")

    app.emit({
        message: "character-infos-loaded",
        infos: await loadCharacterInfos(),
    })

    if (document.location.hash && document.location.hash.length > 0) {
        app.emit({
            message: "load-character",
            id: document.location.hash.substr(1),
        })
    }
})

function initializeModel(): models.Model {
    return {}
}
