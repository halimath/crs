import * as models from "../models"

import { CharacterInfoDto, CharacterDto } from "@swrpgcrs/api"

export async function loadCharacterInfos(): Promise<Array<models.CharacterInfo>> {
    const resp = await fetch("/api/players/00000000-0000-0000-0000-000000000001/characters", {})
    const body = await resp.json() as Array<CharacterInfoDto>

    return body.map(characterInfoFromDto)
}

export async function loadCharacter(id: string): Promise<models.Character> {
    const resp = await fetch(`/api/characters/${id}`, {})
    const dto = await resp.json() as CharacterDto

    return characterFromDto(dto)
}

async function characterFromDto(dto: CharacterDto): Promise<models.Character> {
    const info = characterInfoFromDto(dto.info)

    const brawn = new models.Characteristic(models.CharacteristicKind.Brawn, dto.characteristics.brawn)
    const agility = new models.Characteristic(models.CharacteristicKind.Agility, dto.characteristics.agility)
    const intellect = new models.Characteristic(models.CharacteristicKind.Intellect, dto.characteristics.intellect)
    const cunning = new models.Characteristic(models.CharacteristicKind.Cunning, dto.characteristics.cunning)
    const willpower = new models.Characteristic(models.CharacteristicKind.Willpower, dto.characteristics.willpower)
    const presence = new models.Characteristic(models.CharacteristicKind.Presence, dto.characteristics.presence)

    const resolveCharacteristic = (c: models.CharacteristicKind) => {
        switch (c) {
            case models.CharacteristicKind.Brawn:
                return brawn
            case models.CharacteristicKind.Agility:
                return agility
            case models.CharacteristicKind.Intellect:
                return intellect
            case models.CharacteristicKind.Cunning:
                return cunning
            case models.CharacteristicKind.Willpower:
                return willpower
            case models.CharacteristicKind.Presence:
                return presence
        }
    }

    return new models.Character(
        info,
        [brawn, agility, intellect, cunning, willpower, presence],
        new models.Skills(
            dto.skills.general.map(d => new models.Skill(d.id, resolveCharacteristic(d.characteristic), d.career, d.rank)),
            dto.skills.combat.map(d => new models.Skill(d.id, resolveCharacteristic(d.characteristic), d.career, d.rank)),
            dto.skills.knowledge.map(d => new models.Skill(d.id, resolveCharacteristic(d.characteristic), d.career, d.rank)),
        )
    )
}

function characterInfoFromDto (dto: CharacterInfoDto): models.CharacterInfo {
    return new models.CharacterInfo(dto.id, dto.name, dto.species, dto.careers.map(dto => new models.Career(dto.name, dto.specialization)))
}
