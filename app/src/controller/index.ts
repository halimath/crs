import * as wecco from "@wecco/core"

import { loadCharacter } from "src/api"
import * as models from "../models"

export interface Back{
    message: "back",
}

export interface CharacterLoaded {
    message: "character-loaded",
    character: models.Character,
}

export interface CharacterInfosLoaded {
    message: "character-infos-loaded",
    infos: Array<models.CharacterInfo>,
}

export interface LoadCharacter {
    message: "load-character",
    id: string,
}

export type Message = Back | CharacterInfosLoaded | CharacterLoaded | LoadCharacter

export function update(model: models.Model, message: Message, context: wecco.AppContext<Message>): models.Model {
    console.log(message)

    switch (message.message) {
        case "back":
            history.replaceState(null, "", document.location.href.replace(/#?.*$/, ""))
            model.character = undefined
            break

        case "character-infos-loaded":
            model.characterInfos = message.infos
            break           

        case "character-loaded":
            history.replaceState(null, "", document.location.href.replace(/#?.*$/, `#${message.character.id}`))
            model.character = message.character
            break

        case "load-character":
            loadCharacter(message.id)
                .then(c => {
                    context.emit({message: "character-loaded", character: c})
                })
                // TODO: Handle error
            context.emit
            break
    }

    return model
}