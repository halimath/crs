
export interface Model {
    characterInfos?: Array<CharacterInfo>,
    character?: Character
}

export class Character {
    constructor(public info: CharacterInfo, public characteristics: Array<Characteristic>, public skills: Skills) { }

    get id(): string {
        return this.info.id
    }

    getCharacteristic(kind: CharacteristicKind): Characteristic | undefined {
        return this.characteristics.find(c => c.kind === kind)
    }
}

export class Career {
    constructor (public name: string, public specialization: string) {}
}

export class CharacterInfo {
    constructor (public id: string, public name: string, public species: string, public careers: Array<Career>) {}
}

export interface DicePool {
    ability?: number,
    proficiency?: number,
    difficulty?: number,
    challenge?: number,
    boost?: number,
    setback?: number,
    force?: number,
}

export interface ProvidesDicePool {
    dicePool: DicePool,
}

export enum CharacteristicKind {
    Brawn = "brawn",
    Agility = "agility",
    Intellect = "intellect",
    Cunning = "cunning",
    Willpower = "willpower",
    Presence = "presence"
}

export class Characteristic implements ProvidesDicePool {
    constructor(public kind: CharacteristicKind, public rank: number) { }

    get dicePool(): DicePool {
        return {
            ability: this.rank,
        }
    }
}

export class Skills {
    constructor(public general: Array<Skill>, public combat: Array<Skill>, public knowledge: Array<Skill>) {}
}

export class Skill implements ProvidesDicePool {
    constructor (public id: string, public characteristic: Characteristic, public career: boolean, public rank: number) {}

    get dicePool(): DicePool {
        return {
            proficiency: Math.min(this.rank, this.characteristic.rank),
            ability: Math.abs(this.rank - this.characteristic.rank),    
        }
    }
}

