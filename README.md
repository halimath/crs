
# StarWars Role Playing Game Character Record Sheet

This repo contains a web based app for an online
character record sheet (crs) for the StarWars narrative
role playing game series (such as "Edge of the Empire").

The app consists of two modules: 
* [the web-app](./app)
* [the server side backend](./backend)